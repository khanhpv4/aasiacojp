<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'aasea' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6zX6,]jHU1gt,aWx|fKsukS)[-3*hDtDoUwGJ0tO7UTN{MQJcA~yY(y>?wkoh&nr' );
define( 'SECURE_AUTH_KEY',  '~KVi_=ThW5@rgYjY>_uh1kTuTUTIFf]b4mLE,!0NDx<*u!4OqHh7`(=Aq+_@aR*L' );
define( 'LOGGED_IN_KEY',    '*cPE?1ijP q0Rl1 >3U:4|&yJj_E/7L9C%0@(I4kcOefoL]*h!bQD;9B~NM#PIO*' );
define( 'NONCE_KEY',        'n{s7f@_ChSs?L;wXW#5-i]ajYqUEFGK[=L&*;AA/.hvG)A7ZtnT>FG$S=$e7*!SY' );
define( 'AUTH_SALT',        '}-(TepY8DRT1rV3&E?Z[Ijr[)*$qJH 13Q$?3->A3bF71]oMnY[f_%Z]GD5|*3HQ' );
define( 'SECURE_AUTH_SALT', '`i%/*_SVxJMcyl{-M%X)8c?ljHcXG[bCd#(EBA:)_/R|,^q_hun71XQz{Y^?pW&1' );
define( 'LOGGED_IN_SALT',   'Pdx !#}R[5wGPcBo}SKc8iOc9X2d+24Xy}<ys*ibWfy`h^J?yR[LO^>TI^~P-q~<' );
define( 'NONCE_SALT',       'zpDCc!d-_0S5sNvO+[]u]-xcle$=ny$+k~mI}jZaH?r@?/t35S`PUMd5p`L{;fk-' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
